package app;

import java.util.Random;;

class Helper {
    // ! debug
    boolean debug;

    // ! to get N and phi(N)
    public int[] getBasics() {
        debug = true;

        Random ran = new Random();

        int P, Q;
        // * definition area
        int ranSize = 5;
        int Pmin = (int) Math.pow(2, ranSize); // * = 2^ransSize
        int Pmax = Pmin + (Pmin / 4);

        // ! calc P random
        P = ran.nextInt(Pmax - Pmin) + Pmin; // * P = random
        boolean a = false;
        // * P++ until P is prime
        do {
            P++;
            a = !isPrime(P);
        } while (a);

        // ! calc Q random
        Q = ran.nextInt(Pmin) + Pmax;
        do {
            Q++;
            if (Q == P)
                Q++; // * if area for random to small, it may occur that Q = P -> Q++
            a = !isPrime(Q);
        } while (a);
        
        if(debug)System.out.println("P: " + P + "; Q: " + Q);       //> debug

        // ! N, phiN
        int[] result = new int[2];
        result[0] = P * Q; // ! N
        result[1] = (P - 1) * (Q - 1); // ! phi(N)

        if(debug) System.out.println("N: " + result[0] + "; phi(N): " + result[1]); //> debug

        return result;
    }

    // ! greatest common divisor
    public int gcd(int a, int b) {
        debug = false;
        if (debug)
            System.out.println("gcd: " + a); // > gcd: a

        if (b == 0)
            return a;
        return gcd(b, (a % b));
    }

    // ! a ^ b mod c
    // * a -> result, e -> exponent, b -> radiant, mod -> modul
    // * nur positive Werte übergeben
    public int AmulBmodC(int radiant, int exponent, int modulo) {
        debug = false;
        return aMulbModc_Main(radiant, radiant, exponent, modulo);
    }

    public int aMulbModc_Main(int a, int aa, int e, int mod) {
        if (debug)
            System.out.println("a^b%c: a=" + a + "; e=" + e + "; mod=" + mod); // > a ^ b % c
        if (a == 0)
            return 0;
        else if (e == 0)
            return 1;
        else if (e > 1)
            return aMulbModc_Main((a * aa) % mod, aa, (e - 1), mod);
        else
            return a;
    }

    // ! is prime()
    public boolean isPrime(int a) {
        if (debug) System.out.println("isPrime: " + a); // > isPrime: a

        if (a > 0 && a < 4)
            return true;
        else {
            for (int i = 2; i < a; i++) {
                if (a % i == 0)
                    return false;
            }
        }
        return true;
    }
}